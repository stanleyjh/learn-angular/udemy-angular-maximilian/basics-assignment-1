import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { SuccessAlertComponent } from "./SuccessAlert/success-alert.component";
import { WarningAlertComponent } from "./WarningAlert/warning-alert.component";
import { FailureAlertComponent } from "./WarningAlert/FailureAlert/failure-alert.component";

@NgModule({
  declarations: [
    AppComponent,
    SuccessAlertComponent,
    WarningAlertComponent,
    FailureAlertComponent,
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
