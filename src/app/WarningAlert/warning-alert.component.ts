import { Component } from "@angular/core";

@Component({
  selector: "app-warning-alert",
  template:
    "<h2 class='app-warning-alert'>Warning Alert!</h2><app-failure-alert></app-failure-alert>",
  styleUrls: ["./warning-alert.component.css"],
  styles: [
    `
      .app-warning-alert {
        font-weight: bold;
      }
    `,
  ],
})
export class WarningAlertComponent {}
